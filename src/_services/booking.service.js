import HttpService from "../_services/http.service";
import { APP_SETTINGS } from "../../app.settings";

const MODULE_NAME = "BookingService";

export default {
  name: MODULE_NAME,

  makeBooking(bookingDetails) {
    HttpService.post(APP_SETTINGS.API_ENDPOINTS.BOOKING, bookingDetails);
  }
};
