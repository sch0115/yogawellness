const functions = require('firebase-functions');
const admin =  require("firebase-admin");
admin.initializeApp(functions.config().firebase);

const sgMail = require('@sendgrid/mail');
const cors = require("cors")({origin: true});

//Emailing part
const SEND_GRID_KEY = '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';

const EMAIL_SENDER_CONFIRMATION = 'wellnesstravels@hotmail.com';
const EMAIL_SUBJECT_CONFIRMATION = 'Booking confirmation - Yoga on Bali';
const EMAIL_BODY_CONFIRMATION = 'Thank you for your email! \n We will be in touch with you shortly';


const BOOKING_EMAIL_RECIEVER = 'wellnesstravels@hotmail.com';
// const BOOKING_EMAIL_RECIEVER =  'adam.sch0115@gmail.com';
const BOOKING_EMAIL_SUBJECT = 'NEW BOOKING FROM WEB';


exports.makeBooking = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        const bookingDetails = request.body;
        bookingDetails.bookingTimestamp = new Date();

        console.log('Got booking request');
        console.log(bookingDetails);

        // // SEND CONFIRMATION EMAIL
        // sendEmail(bookingDetails.email, EMAIL_SENDER_CONFIRMATION, EMAIL_SUBJECT_CONFIRMATION, EMAIL_BODY_CONFIRMATION);
        //SEND NOTIFICATION EMAIL TO OWNER
        sendEmail(BOOKING_EMAIL_RECIEVER, bookingDetails.email, BOOKING_EMAIL_SUBJECT, JSON.stringify(bookingDetails));
        //SEND RESPONSE
        response.status(204).json({
            message: 'Message has been send'
        });
    });
});

function sendEmail(to, from, subject, text, html) {
    sgMail.setApiKey(SEND_GRID_KEY);

    const msg = {
        to,
        from,
        subject,
        text,
        html
    };

    console.log('Sending email');
    console.log(msg);
    sgMail.send(msg);
}
