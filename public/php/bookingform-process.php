<?php
$errorMSG = "";

if (empty($_POST["completename"])) {
    $errorMSG = "Complete name required ";
} else {
    $completename = $_POST["completename"];
}

if (empty($_POST["nrofrooms"])) {
    $errorMSG = "Number of rooms required ";
} else {
    $nrofrooms = $_POST["nrofrooms"];
}

if (empty($_POST["nrofpeople"])) {
    $errorMSG .= "Number of people required ";
} else {
    $nrofpeople = $_POST["nrofpeople"];
}

if (empty($_POST["phonenr"])) {
    $errorMSG .= "Phone number required ";
} else {
    $phonenr = $_POST["phonenr"];
}

if (empty($_POST["email"])) {
    $errorMSG .= "Email is required ";
} else {
    $email = $_POST["email"];
}

if (empty($_POST["start"])) {
    $errorMSG .= "Check-in date required ";
} else {
    $start = $_POST["start"];
}

if (empty($_POST["end"])) {
    $errorMSG .= "Check-out date required ";
} else {
    $end = $_POST["end"];
}


$EmailTo = "yourname@domain.com";
$Subject = "New message from Villa landing page registration form";
// prepare email body text
$Body = "";
$Body .= "Complete name: ";
$Body .= $completename;
$Body .= "\n";
$Body .= "Number of rooms: ";
$Body .= $nrofrooms;
$Body .= "\n";
$Body .= "Number of people: ";
$Body .= $nrofpeople;
$Body .= "\n";
$Body .= "Phone number: ";
$Body .= $phonenr;
$Body .= "\n";
$Body .= "Email: ";
$Body .= $email;
$Body .= "\n";
$Body .= "Check-in date: ";
$Body .= $start;
$Body .= "\n";
$Body .= "Check-out date: ";
$Body .= $end;
$Body .= "\n";

// send email
$success = mail($EmailTo, $Subject, $Body, "From:".$email);
// redirect to success page
if ($success && $errorMSG == ""){
   echo "success";
}else{
    if($errorMSG == ""){
        echo "Something went wrong :(";
    } else {
        echo $errorMSG;
    }
}
?>